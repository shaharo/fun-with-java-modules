package com.shaharoliver.app.client;

import com.shaharoliver.server.NumberServer;

import java.util.ServiceLoader;

public class Main {



    public static void main(String[] args) {
        ServiceLoader<NumberServer> loader = ServiceLoader.load(NumberServer.class);

        for (NumberServer s: loader) {
            for(int i=0;i<10;i++) {
                System.out.printf("Got Number %s from %s \n ",s.getNum(),s.name());
            }

        }
    }
}
