import com.shaharoliver.server.NumberServer;
import com.shaharoliver.server.doubleserver.DoubleServer;

module com.shaharoliver.server.doubleserver {
    requires com.shaharoliver.server;

    exports  com.shaharoliver.server.doubleserver;

    provides NumberServer
            with DoubleServer;
}