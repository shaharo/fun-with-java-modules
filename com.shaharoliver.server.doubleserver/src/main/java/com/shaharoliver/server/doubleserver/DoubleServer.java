package com.shaharoliver.server.doubleserver;

import com.shaharoliver.server.NumberServer;
import com.shaharoliver.server.doubleserver.internal.DoubleGenerator;

public class DoubleServer implements NumberServer {

    @Override
    public String getNum() {
        return DoubleGenerator.randomDouble(5.5d,20d);
    }

    @Override
    public String name() {
        return "Double Server";
    }
}
