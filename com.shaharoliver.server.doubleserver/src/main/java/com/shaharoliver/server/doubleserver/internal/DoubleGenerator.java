package com.shaharoliver.server.doubleserver.internal;

public class DoubleGenerator {

    public static String randomDouble(Double min , Double max) {
        Double d = min + Math.random() * (max - min);
        return String.valueOf(d);
    }
}
