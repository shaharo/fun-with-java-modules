import com.shaharoliver.server.NumberServer;
import com.shaharoliver.server.integerserver.IntServer;

module com.shaharoliver.server.integerserver {
    requires com.shaharoliver.server;
    exports com.shaharoliver.server.integerserver;

    provides NumberServer
            with IntServer;
}