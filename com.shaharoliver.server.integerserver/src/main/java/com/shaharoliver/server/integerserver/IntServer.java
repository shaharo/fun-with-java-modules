package com.shaharoliver.server.integerserver;

import com.shaharoliver.server.NumberServer;
import com.shaharoliver.server.integerserver.internal.IntegerGenerator;

public class IntServer implements NumberServer {


    @Override
    public String getNum() {
        return IntegerGenerator.randomInteger(0, 100);
    }

    @Override
    public String name() {
        return "Integer Server";
    }


}
