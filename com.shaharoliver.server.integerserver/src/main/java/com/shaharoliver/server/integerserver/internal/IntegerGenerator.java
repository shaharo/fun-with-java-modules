package com.shaharoliver.server.integerserver.internal;

public class IntegerGenerator {

    public static String randomInteger(Integer min , Integer max) {
        Integer d = ((int) (Math.random()*(max - min))) + min;
        return String.valueOf(d);
    }

}
